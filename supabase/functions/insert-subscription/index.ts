import { createClient } from 'https://esm.sh/@supabase/supabase-js@2';

const supabaseUrl = Deno.env.get('SUPABASE_URL') ?? '';
const supabaseAnonKey = Deno.env.get('SUPABASE_ANON_KEY') ?? '';
const supabaseClient = createClient(supabaseUrl, supabaseAnonKey);

const handleCors = (req: Request) => {
  if (req.method === 'OPTIONS') {
    return corsResponse('OK');
  }
  return null;
};

const handlePostRequest = async (req: Request) => {
  const formData = await req.formData();
  const name = formData.get('name');
  const phone = formData.get('phone');
  const days = formData.get('days');
  const files = formData.getAll('files');
  const order = formData.get('order');

  const filesUploadResult = await uploadFiles(files);
  const data = { name, phone, photo: filesUploadResult, days: parseInt(days), order: parseInt(order) }; // Use the client-side generated order number

  const insertResult = await insertDataIntoSupabase(data);
  return insertResult;
};

const uploadFiles = async (files: File[]) => {
  const filenames = files.map((file) => {
    const filename = `${Math.floor(1000000 + Math.random() * 9000000)}.${file.name.split('.').pop()}`;
    return { file, filename };
  });

  const fileUploadPromises = filenames.map(({ file, filename }) => {
    return supabaseClient.storage.from('Photos').upload(`public/${filename}`, file, {
      cacheControl: '3600',
      upsert: false,
    });
  });

  await Promise.all(fileUploadPromises);
  return filenames.map(({ filename }) => filename);
};

const insertDataIntoSupabase = async (data: any) => {
  const { data: insertedData, error } = await supabaseClient
    .from('Test')
    .insert(data, { returning: 'representation' });

  if (error) {
    return corsResponse(JSON.stringify({ message: 'Error inserting data' }), 'application/json', 500);
  }

  return corsResponse(JSON.stringify({ message: 'Data inserted successfully!', insertedData }), 'application/json');
};

const corsResponse = (body: string, contentType = 'text/plain', status = 200) => {
  return new Response(
    body,
    {
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': 'Content-Type, Authorization',
        'Content-Type': contentType,
      },
      status,
    },
  );
};

Deno.serve(async (req) => {
  let response;
  response = handleCors(req);
  if (!response) {
    response = await handlePostRequest(req);
  }
  return response;
});